import sys
import bundler
bundler.init()
from workflow import Workflow, ICON_WARNING, web


class ClojuredocsWorkflow(object):
    """Represents a Workflow that connects to clojuredocs.org and
    returns items found that match the given query string."""
    def __init__(self, wf):
        super(ClojuredocsWorkflow, self).__init__()
        self.wf = wf
        self.q = wf.args[0].strip()

    def _internalize(self, clojuredocs_datapoint):
        """Converts `Keyword` objects found in the output of edn_format to regular dictionary keys."""
        return {datakey.name: map(self._internalize, dataval) if type(dataval) in (tuple, list) else dataval
                for datakey, dataval in clojuredocs_datapoint.items()}

    def _fetch(self):
        """Connects to clojuredocs.org and returns parsed EDN (Extensible Data Notation).
        Field structure returned:
        {Keyword(doc): u'',
        Keyword(file): u'',
        Keyword(arglists): u'',
        Keyword(keywords): u'',
        Keyword(see-alsos): ({Keyword(library-url): u'', Keyword(ns): u'', Keyword(name): u''},
                             {Keyword(library-url): u'', Keyword(ns): u'', Keyword(name): u''},
                             {Keyword(library-url): u'', Keyword(ns): u'', Keyword(name): u''},
                             {Keyword(library-url): u'', Keyword(ns): u'', Keyword(name): u''}),
        Keyword(name): u'',
        Keyword(ns): u'',
        Keyword(column): u'',
        Keyword(macro): u'',
        Keyword(added): u'',
        Keyword(edit-distance): 1,
        Keyword(type): u'',
        Keyword(line): u'',
        Keyword(href): u'',
        Keyword(examples-count): 4,
        Keyword(library-url): u''}
        """
        import edn_format
        url = 'http://clojuredocs.org/ac-search'
        params = {'query': self.q}
        r = web.get(url, params)
        r.raise_for_status()
        return edn_format.loads(r.text)

    def _lookup(self):
        """Wrapper for `_fetch` and `_internalize` for caching purposes. Cache is indefinite (-1)."""
        refresh = lambda: [self._internalize(datapoint) for datapoint in self._fetch()]
        return self.wf.cached_data(self.q, refresh, max_age=-1)

    def _icon_for_type(self, obj_type):
        """Returns the path to an icon file that represents the specified object
        type as returned by web API."""
        if obj_type in ('function', 'macro', 'var'):
            return 'icons/%s.png' % obj_type

    def _title(self, obj_name, obj_type, obj_ns):
        if obj_type == 'namespace':
            return '%s [%s]' % (obj_name, obj_type)
        return '%s [%s in %s]' % (obj_name, obj_type, obj_ns)

    def run(self):
        """Main execution body of class and indeed the file. Gets the result set from the
        web/cache and iterates through it, adding entries to the Alfred result list."""
        data = self._lookup()
        if not data:
            self.wf.add_item('No matches', valid=False, icon=ICON_WARNING)

        for func_desc in data:
            kw = lambda k: func_desc.get(k, '')
            title = self._title(kw('name'), kw('type'), kw('ns'))
            subtitle = kw('doc')
            arg = 'http://clojuredocs.org%s' % kw('href')
            autocomplete = kw('name')
            uid = 'clojuredocs-%s/%s' % (kw('ns'), kw('name'))
            icon = self._icon_for_type(kw('type'))
            self.wf.add_item(title, subtitle, arg=arg, autocomplete=autocomplete, valid=True, uid=uid, icon=icon)

        self.wf.send_feedback()


if __name__ == '__main__':
    wf = Workflow()
    log = wf.logger
    decode = wf.decode
    sys.exit(wf.run(lambda wf: ClojuredocsWorkflow(wf).run()))
